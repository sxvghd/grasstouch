package main

import (
	"context"
	"crypto/tls"
	"encoding/xml"
	"log"
	"mellium.im/sasl"
	"mellium.im/xmlstream"
	"mellium.im/xmpp"
	"mellium.im/xmpp/dial"
	"mellium.im/xmpp/jid"
	"mellium.im/xmpp/stanza"
	"strings"
	"strconv"
	"regexp"
)

type TextMessage struct {
	stanza.Message
	Body string `xml:"body"`
}

var session *xmpp.Session
var ctx context.Context
var cancel context.CancelFunc

func HandleMessage(t xmlstream.TokenReadEncoder, start *xml.StartElement) {
	d := xml.NewTokenDecoder(t)
	var msg TextMessage 
	d.DecodeElement(&msg, start)
	if msg.Body == "" {
		return
	}
	cmd := strings.Split(msg.Body," ")
	switch cmd[0] {
		case "help":
			SendMessage(msg.From,"add <fb url> - add place to watchlist\nlist - list places on the watchlist\nremove <fb id> - remove place from the watchlist\nhelp - this help message")
		case "add":
			if len(cmd) < 2 {
				SendMessage(msg.From,"No URL given!")
				return
			}
			exp := regexp.MustCompile("https?://(?:m\\.|www\\.)?facebook.com/([a-zA-Z0-9.]+)/?")
			if exp.MatchString(cmd[1]) {
				id := exp.FindStringSubmatch(msg.Body)[1]
				events, _ := GetEventsForID(id)
				if len(events) == 0 {
					AddPlace(msg.From.Bare().String(),id,0)
				} else {
					AddPlace(msg.From.Bare().String(),id,events[0].ID)
				}
				SendMessage(msg.From,"Place added, commencing event dump!")
				for _, event := range events {
					SendEvent(msg.From,event)
				}
			} else {
				SendMessage(msg.From,"Incorrect facebook page url given!")
			}
		case "list":
			places := GetPlaces(msg.From.Bare().String())
			out := "Page IDs:\n"
			for _, place := range places {
				out += "- "+place.FBID+"\n"
			}
			out = out[:len(out)-1]
			SendMessage(msg.From,out)
		case "remove":
			if len(cmd) < 2 {
				SendMessage(msg.From,"No ID given!")
				return
			}
			if DeletePlace(msg.From.Bare().String(),cmd[1]) != nil {
				SendMessage(msg.From,"Error removing place "+cmd[1]+" from watchlist!")
				return
			}
			SendMessage(msg.From,"Successfully removed "+cmd[1]+" from watchlist!")
	}
}

func SendEvent(to jid.JID, event FBEvent) {
	msg := event.Title+"\n"+event.Date+" "+event.Time+"\n"+event.Place+"\nMore info at https://facebook.com/events/"+strconv.Itoa(event.ID)
	SendMessage(to, msg)
}

func SendMessage(to jid.JID, text string) {
	var msg TextMessage = TextMessage{Message: stanza.Message{From: jid.MustParse(BotJID), To: to.Bare()}, Body: text}
	session.Encode(ctx, msg)
}

func StartXMPP() {
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel()
	var err error
	dialer := dial.Dialer{NoLookup: true}
	conn, err := dialer.Dial(ctx, "tcp", jid.MustParse(BotJID))
	if err != nil {
		log.Fatalf("Error connecting to %s: %v", BotServer, err)
	}
	negotiator := xmpp.NewNegotiator(func(*xmpp.Session, *xmpp.StreamConfig) xmpp.StreamConfig {
		return xmpp.StreamConfig{
			Features: []xmpp.StreamFeature{
				xmpp.StartTLS(&tls.Config{
					ServerName: BotServer,
				}),
				xmpp.SASL("", BotPassword, sasl.ScramSha256Plus, sasl.ScramSha1Plus, sasl.ScramSha256, sasl.ScramSha1, sasl.Plain),
				xmpp.BindResource(),
			},
		}
	})
	session, err = xmpp.NewSession(context.TODO(), jid.MustParse(BotServer), jid.MustParse(BotJID), conn, 0, negotiator)
	if err != nil {
		log.Fatalf("Error logging in as %s: %v", BotJID, err)
	}
	log.Println("Connected to", BotServer, "as", BotJID)
	err = session.Send(ctx, stanza.Presence{Type: stanza.AvailablePresence}.Wrap(nil))
	if err != nil {
		log.Fatalf("Error sending initial presence: %w", err)
	}
	err = session.Serve(xmpp.HandlerFunc(func(t xmlstream.TokenReadEncoder, start *xml.StartElement) error {
		switch start.Name.Local {
		case "message":
			HandleMessage(t, start)
		}
		return nil
	}))
}
