# grasstouch
An XMPP bot notifying you of new public FB events.

Helps with getting rid of those pesky invasive services :)

### Building
`go build grasstouch`

### Usage
To get `grasstouch` running, you need to make an account for it on a server of your choice and then pass the credentials for it to connect and function.

Example:
`./grasstouch -jid fbevent@localhost -pwd password -server localhost`

A complete description of all available flags is accessible under `-h` or `--help`

After connecting, messaging the bot allows you to add/remove/list tracked pages/places, more info can be obtained by sending the `help` command.

### Disclaimer
The bot notifies you about new events by parsing the new Facebook mobile event page. This page usually gets blocked and requires login when accessed from a non-residential IP address, so it is recommended to run `grasstouch` from your home or something.