package main

import (
	"flag"
	"time"
	"log"
	"mellium.im/xmpp/jid"
)

var ProbeTime int
var DBPath string
var BotJID string
var BotPassword string
var BotServer string

func StartTicker() {
	ticker := time.NewTicker(time.Duration(ProbeTime) * time.Hour)
	for _ = range ticker.C {
		users, err := GetUsers()
		if err != nil {
			log.Fatal("Error retrieving users")
			continue
		}
		for _, user := range users {
			for i, place := range user.Places {
				events, _ := GetEventsForID(place.FBID)
				for _, event := range events {
					if event.ID == place.LastEvent {
						break
					}
					SendEvent(jid.MustParse(user.JID),event)
				}
				user.Places[i].LastEvent = events[0].ID
			}
			SetUser(user)
		}
	}
}

func ParseArgs() {
	flag.StringVar(&DBPath, "db", "./db", "DB location folder")
	flag.StringVar(&BotJID, "jid", "", "XMPP account the bot will log into and utilize")
	flag.StringVar(&BotPassword, "pwd", "", "Password for the account to be utilized")
	flag.StringVar(&BotServer, "server", "localhost", "Server to log into")
	flag.IntVar(&ProbeTime, "refresh", 6, "The duration (in hours) between checking for new events")
	flag.Parse()
}

func main() {
	ParseArgs()
	if BotJID == "" || BotPassword == "" || BotServer == "" {
		log.Fatal("No credentials given!")
	}
	InitDB()
	go StartTicker()
	StartXMPP()
}
