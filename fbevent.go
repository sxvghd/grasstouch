package main

import (
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
)

type FBEvent struct {
	ID       int
	Title    string
	Time     string
	Date     string
	Place    string
	Location string
}

func GetEventsForID(pageID string) ([]FBEvent, error) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, "https://m.facebook.com/"+pageID+"/events", nil)
	if err != nil {
		return []FBEvent{}, err
	}
	req.Header.Add("user-agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Mobile Safari/537.36")
	req.Header.Add("authority", "m.facebook.com")
	req.Header.Add("pragma", "no-cache")
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("upgrade-insecure-requests", "1")
	req.Header.Add("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'")
	req.Header.Add("sec-fetch-site", "none")
	req.Header.Add("sec-fetch-mode", "navigate")
	req.Header.Add("sec-fetch-user", "?1")
	req.Header.Add("sec-fetch-dest", "document")
	req.Header.Add("accept-language", "en-US,en;q=0.9")
	req.Header.Add("sec-ch-ua", `" Not A;Brand";v="99", "Chromium";v="98"`)
	req.Header.Add("sec-ch-ua-mobile", "?1")
	req.Header.Add("sec-ch-ua-platform", `"Android"`)
	resp, err := client.Do(req)
	if err != nil {
		return []FBEvent{}, err
	}
	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var out []FBEvent
	pattern := regexp.MustCompile(`<h3 class=".+?">(.+?)<\/h3><span class=".+?"><span class=".+?"><span class=".+?">(\w+)<\/span><span class=".+?">(\d+)<\/span><\/span><\/span><div class=".+?"><span class=".+?"><span title=".+?">(.+?)<\/span><\/span><\/div><div class=".+?"><span class=".+?">(.+?)<\/span><\/div><div class=".+?"><div><\/div><\/div><a class=".+?" href="\/events\/(\d+)\?.+?" aria-label="View event details for .+?"><\/a>`)
	matches := pattern.FindAllStringSubmatch(string(respBytes), -1)
	for _, match := range matches {
		if len(match) < 6 {
			log.Println("Error parsing event for pageID", pageID)
			continue
		}
		event := FBEvent{Title: html.UnescapeString(match[1]), Date: match[2] + " " + match[3], Place: html.UnescapeString(match[5]), Time: match[4]}
		event.ID, err = strconv.Atoi(match[len(match)-1])
		if err != nil {
			log.Println("Error parsing ID for event from pageID", pageID)
			continue
		}
		out = append(out, event)
	}
	return out, nil
}
