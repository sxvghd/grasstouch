package main

import (
	"encoding/json"
	"github.com/abhinav-TB/dantdb"
)

type Place struct {
	FBID      string
	LastEvent int
}

type User struct {
	JID    string
	Places []Place
}

var db *dantdb.Driver

func InitDB() error {
	var err error = nil
	db, err = dantdb.New(DBPath)
	return err
}

func AddPlace(userJID string, place string, lastevent int) {
	var user User
	db.Read("users", userJID, &user)
	user.JID = userJID
	user.Places = append(user.Places, Place{place, lastevent})
	db.Write("users", userJID, user)
}

func SetUser(user User) {
	db.Write("users", user.JID, user)
}

func GetPlaces(userJID string) []Place {
	var user User
	db.Read("users", userJID, &user)
	return user.Places
}

func GetUsers() ([]User, error) {
	records, err := db.ReadAll("users")
	if err != nil {
		return []User{}, err
	}

	var users []User

	for _, record := range records {
		var user User
		json.Unmarshal([]byte(record), &user)
		users = append(users, user)
	}
	return users, nil
}

func DeletePlace(userJID string, place string) error {
	var user User
	err := db.Read("users", userJID, &user)
	if err != nil {
		return err
	}
	for i, elem := range user.Places {
		if elem.FBID == place {
			if len(user.Places) > 1 {
				if i+1 == len(user.Places) {
					user.Places = user.Places[:i]
				} else {
					user.Places = append(user.Places[:i], user.Places[i+1])
				}
			} else {
				user.Places = []Place{}
			}
		}
	}
	db.Write("users", userJID, user)
	return nil
}
