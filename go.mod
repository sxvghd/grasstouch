module grasstouch

go 1.18

require github.com/abhinav-TB/dantdb v0.0.0-20220522153004-c2602a1cc142

require (
	golang.org/x/crypto v0.0.0-20220817201139-bc19a97f63c8 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220812174116-3211cb980234 // indirect
	golang.org/x/sys v0.0.0-20220817070843-5a390386f1f2 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.12 // indirect
	mellium.im/reader v0.1.0 // indirect
	mellium.im/sasl v0.3.0 // indirect
	mellium.im/xmlstream v0.15.4-0.20211023152852-0ca80a938137 // indirect
	mellium.im/xmpp v0.21.3 // indirect
)
